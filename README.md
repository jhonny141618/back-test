# Nabenik's Enterprise Java basic test

Hi and welcome to this test. As many technical interviews, main test objective is to establish your actual Enterprise coding skills, being:

* Java knowledge
* DDD knowledge
* General toolkits, SDK's and other usages
* Jakarta EE general skills

To complete this test, please create a fork of this repository, fix the source code if required, add your answers to this readme file, commit the solutions/answers to YOUR copy and create a pull request to the original repo.

This document is structured using [GitHub Markdown Flavor](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code).

## General questions

1. How to answer these questions?

> Like this

Or maybe with code

```kotlin
fun hello() = "world"
```

2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations. Is this project using all specs?

- EJB
> Es una api que encapsula cierta logica del negocio, de esta manera
> se ejecuta en un entorno de ejecución para ser accedida de otros componentes.
- Servlet
>Es un componente del lado del servidor para la construcción de paginas web
> dinamicas.
- CDI
> Es un componente que permite la inyeccion de objetos dentro de otros de esta manera se 
> solo nos enfocamos en indicar lo que se necesita delegando la responsabilidad al CDI. 
- JAX-RS
> Es una api que incorpora caracteristicas que facilitan la crearcion de servicios web basado en la arquitectura REST.

3. Which of the following is an application server?

* Open Liberty
* Apache TomEE
* Eclipse Jetty
* Eclipse Glassfish
* Oracle Weblogic

4. In your opinion, what's the main benefit of moving from Java 11 to Java 17?
> Java 17 proporciona una mayor automización en la ejecución y siempre se agregan nuevas mejoras que ayudan a optimizar el desarrollo y reduccir costos.
5. Is it possible to run this project (as is) over Java 17? Why?
> No, ya que como se menciono esta construido con Java EE y oracle dejo de darle soporte ya hace unos años.
6. Is it possible to run this project with GraalVM Native? Why?
> Si, debido a que es una distribución mas del jdk aunque pueda que no sea una de las mejores opciones.
7. How do you run this project directly from CLI without configuring any application server? Why is it possible?
> En el estado inicial que se me brindo no, por falta de configuración, pero si lo estuviera si es posible debido que se usa maven y al final lo que hace es facilitar procesos en este caso de ejecución que son transformados a comandos de terminal al final.
## Development tasks

To solve this questions please use Gitflow workflow, still, your answers should be in the current branch.

Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).

0. (easy) Show your terminal demonstrating your installation of OpenJDK 11

1. (easy) Run this project using an IDE/Editor of your choice

2. (medium) Execute the movie endpoint operations using curl, if needed please also check/fix the code to be REST compliant

3. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer

4. (medium) This project has been created using Java EE APIs, please move it to Jakarta EE APIs and switch it to a compatible implementation (if needed)

5. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods

6. (hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test

7. (hard) This project uses vanilla Java EE for Database Persistence, please integrate Testcontainers with MySQL for testing purposes

8. (nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Open Liberty](https://openliberty.io/). Do it and don't port Integration Tests 

